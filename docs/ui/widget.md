# Widget

[Главная](../main.md)
[Структура UI](structure.md)

При использовании WM должен использоваться StatefulWidget.

State такого виджета должен быть наследником `WidgetState<>`.
Этот класс, расширяет State и добавляет автоматическую отписку потоков внутри модели,
а также автоматически связывает и конфигурирует WM.

Для конфигурации зависимостей для экрана(виджета) необходимо переопределить метод 
`getComponent()` у WidgetState, в котором указать необходимый экрану `BaseWidgetModelComponent`.
Также WidgetState необходимо типизировать:
1. Widget - виджет, котрому принадлежит стейт
1. WidgetModel - тип WM, который используется в виджете.
1. WidgetComponent - тип  компонента ждля внедрения зависимостей на виджет.

**Важно**: 

- При использовании `Injector` правильно выбирайте context.

- В Widget не должно попадать экранной логики. В этой сущности происходит только связывание 
элементов пользовательского интерфейса в WidgetModel.

*Примечания*:
- Widget - это любой виджет из Flutter.
- Для подписки на изменение модели используется StreamBuilder.